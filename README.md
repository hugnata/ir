# Intelligent Robotics at UoB

Repo for Intelligent Robotics labs at the university of Birmingham. 

## Lab Package

The lab package contains all the script written for the purpose of the lab

* robot_control.py: Allow the robot to explore the map using only avoiding obstacles

## Socspioneer Package

Package given by the university of birmingham. Contains scripts and map data. 

# Instalation 

* Clone the git repo `git clone https://gitlab.com/hugnata/ir/`
* Go in the repo folder `cd ir`
* Initialize the submodule `git submodule init`
* Update the submodule `git submodule update`

See Socspioneer Readme for the install of the requirements

# Running the scene

To launch the world scene: 

1. Set ros environment variables: `source /opt/ros/melodic/setup.bash`
2. Source the catkin_ws setup script: `source <catkin_ws>/devel/setup.bash`
3. Launch ros core in background: `nohup roscore &`
4. Launch simulation `rosrun stage_ros stageros <catkin_ws>/src/socspioneer/data/meeting.world`
5. Launch the robot controller in another terminal (repeat steps 1 & 2 in the new terminal): `rosrun lab robot_controller.py`






