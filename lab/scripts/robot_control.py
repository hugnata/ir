#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#
# Revision $Id$

## Simple talker demo that published std_msgs/Strings messages
## to the 'chatter' topic

import rospy
from geometry_msgs.msg import Twist, Vector3
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
import random

last_ray = 0

def callback(data):
	global last_ray
	last_ray = min(data.ranges[len(data.ranges)/2-20:len(data.ranges)/2+20])
	

def talker():
	pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
	global last_ray
	rate = rospy.Rate(1) # 10hz
	while not rospy.is_shutdown():
		if last_ray < 2:
			pub.publish(Twist(Vector3(0,0,0),Vector3(0,0,1)))
		else:
			pub.publish(Twist(Vector3(2,0,0),Vector3(0,0,0)))
        rate.sleep()

if __name__ == '__main__':
    try:
		rospy.init_node('talker', anonymous=True)
		rospy.Subscriber('base_scan', LaserScan, callback)
		talker()
    except rospy.ROSInterruptException:
        pass
